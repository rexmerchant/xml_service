# XMLService for IBM iSeries

## XMLService API with Ruby

* * *
***Example of xmlservice with active record running on IBMi.***

***Run a CMD to obtain system time.***

	require 'active_support'
	require 'active_record'
	require 'xmlservice'
	require 'ibm_db'
	require 'yaml'
	
	data=YAML::load(File.read("credentials.yml"))
	
	user = data["user"]
	password = data["password"]
	
	ActiveRecord::Base.establish_connection adapter: 'ibm_db', database: '*LOCAL', username: "#{user}",password: "#{password}"
	
	ActiveXMLService::Base.establish_connection connection: 'ActiveRecord'
	
	cmd = XMLService::I_SH.new("system -i 'WRKSYSVAL SYSVAL(QTIME) OUTPUT(*PRINT)'")
	cmd.xmlservice
	puts cmd.out_xml

credentials.yml

	user: username
	password: pw

Output

	<?xml version='1.0'?>
	<myscript>
	<sh error='fast'>
	<![CDATA[ System Values Page 1
	5770SS1 V7R1M0 100423 S10746FP 03/26/16 14:59:00 UTC
	 Current Shipped
	 Name value value Description
	 QTIME 14:59:00 ' ' Time of day
	 Note: > means current value is different from the shipped value
	 * * * * * E N D O F L I S T I N G * * * * *]]>
	</sh>
	</myscript>

* * *
***Example of active_record running on IBMi.***

***Create a new table called Employees in library (aka schema) RXTEST; then read the table.***

	# Create new Employees table, then read it
	
	require 'active_record'
	require 'yaml'
	
	data=YAML::load(File.read("credentials.yml"));
	
	user = data["user"]
	password = data["password"]
	
	ActiveRecord::Base.establish_connection(
	    :adapter => 'ibm_db',
	    :username=> "#{user}",
	    :password =>  "#{password}",
	    :database => '*LOCAL',
	    :schema => 'rxtest'
	)
	
	# Delete the table if it already exists
	
	ActiveRecord::Migration.drop_table(:employees)
	
	# Create a table in the schema
	
	keepGoing = TRUE
	
	if keepGoing
	
	ActiveRecord::Schema.define do
	
	  create_table :employees do |table|
	    table.column :emp_id, :integer
	    table.column :firstname, :string, limit: 30
	    table.column :lastname, :string, limit: 50
	    table.column :address, :string, limit: 60
	    table.column :city, :string, limit: 40
	    table.column :st, :string, limit: 10
	    table.column :zip, :string, limit: 9
	  end
	end
	
	end
	
	class Employee < ActiveRecord::Base
	
	end
	
	emp = Employee.create(:emp_id => 21,:firstname => 'Arthur', :lastname=>'Kellogg',:address=>'3412 Main St',:city=>'Charlotte',:st=>'NC',:zip=>'28111')
	emp = Employee.create(:emp_id => 25,:firstname => 'Charles', :lastname=>'Johnson',:address=>'831 East M St',:city=>'Charlotte',:st=>'NC',:zip=>'28210')
	emp = Employee.create(:emp_id => 34,:firstname => 'Buddy', :lastname=>'Harrell',:address=>'3211 S Tryon St',:city=>'Charlotte',:st=>'NC',:zip=>'28211')
	
	## Alternate method
	emp = Employee.new
	
	emp.emp_id = 44
	emp.firstname = 'Charles'
	emp.lastname='Clark'
	emp.address = '999 Third Ave'
	emp.city='Charlotte'
	emp.st='NC'
	emp.zip='28251'
	
	emp.save
	
	puts '-------------------------------'
	puts Employee.find_by_id(1).lastname
	puts Employee.find_by_emp_id(44).lastname
	puts Employee.find_by_firstname('Buddy').lastname
	puts Employee.find_by_lastname('Johnson').firstname
	puts '-------------------------------'
	
	Employee.find_each do |empl|
	  puts empl.firstname << ' ' << empl.lastname
	end
	puts '-------------------------------'
	
	Employee.where("firstname = 'Charles'").find_each do |empl|
	  puts empl.firstname << ' ' << empl.lastname
	end
	puts '-------------------------------'

credentials.yml

	user: username
	password: pw

Output

	-- drop_table(:employees)
	 -> 3.6499s
	-- create_table(:employees)
	 -> 4.7626s
	-------------------------------
	Kellogg
	Clark
	Harrell
	Charles
	-------------------------------
	Arthur Kellogg
	Charles Johnson
	Buddy Harrell
	Charles Clark
	-------------------------------
	Charles Johnson
	Charles Clark
	-------------------------------
	
	* * *
***Example of xmlservice calling RPG program with input and output parameters.***

***This is a silly example:  calling an RPG program to laboriously calculate the length of a string.  But it demonstrates the use of input and output parameters.***

	require 'xmlservice'
	require 'yaml'
	
	data=YAML::load(File.read("credentials.yml"));
	
	user = data["user"]
	password = data["password"]
	
	ActiveXMLService::Base.establish_connection(
	    connection: 'ActiveRecord', adapter: 'ibm_db', database: '*LOCAL',
	    username: "#{user}", password: "#{password}"
	)
	
	# Set library list
	
	pgm = XMLService::I_PGM.new('BAILIBL', 'RXTEST')
	pgm.xmlservice
	
	# Calculate string length
	
	inString = "The quick brown fox"
	
	pgm = XMLService::I_PGM.new('APSBS25', '*LIBL') <<
	      XMLService::I_String.new('outReturnCode',7,' ') <<
	      XMLService::I_String.new('inString',256,inString) <<
	      XMLService::I_PackedDecimal.new('outStringLength',5,0,0)
	
	pgm.xmlservice
	
	puts ""
	puts ""
	puts "input String: #{pgm.input.PARM1.inString}"
	puts "-------------------------------------------"
	puts "output returnCode: #{pgm.response.PARM0.outReturnCode}"
	puts "output stringLength: #{pgm.response.PARM2.outStringLength}"
	puts ""


Output

	input String: The quick brown fox
	-------------------------------------------
	output returnCode: 
	output stringLength: 19.0